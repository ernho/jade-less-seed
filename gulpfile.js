
var gulp = require('gulp'),
   uglify = require('gulp-uglify'),
 jade = require('gulp-jade'),
        less = require('gulp-less'),
    livereload = require('gulp-livereload'),
    watch = require('gulp-watch');
 
gulp.task('templates', function() {
  var LOCALS = {};
 
  gulp.src('./lib/*.jade')
    .pipe(jade({
      locals: LOCALS
    }))
    .pipe(gulp.dest('./build/'))
});



gulp.task('minify', function () {
   gulp.src('js/*.js')
      .pipe(uglify())
      .pipe(gulp.dest('build/js'))
});





gulp.task('less', function() {
   gulp.src('less/*.less')
      .pipe(watch())
      .pipe(less())
      .pipe(gulp.dest('build/css'))
      .pipe(livereload());
});


gulp.task('default', function () {
   // Your default task
});